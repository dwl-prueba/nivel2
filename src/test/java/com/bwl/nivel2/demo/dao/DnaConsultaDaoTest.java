package com.bwl.nivel2.demo.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.bwl.nivel2.demo.entity.DnaConsulta;
@RunWith(SpringRunner.class)
@DataJpaTest
class DnaConsultaDaoTest {

	@Autowired
    private TestEntityManager entityManager;
 
    @Autowired
    private DnaConsultaDao dnaConsultaDao;
	
	@Test
	void test() {
		DnaConsulta dna = new DnaConsulta();
		dna.setDna("ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCACTG");
		dna.setFechaAlta(new Date());
		dna.setMutacion(true);
		entityManager.persist(dna);
		entityManager.flush();
		
		
		Integer conteo = dnaConsultaDao.countByMutacionIsTrue();
		assertThat(conteo).isGreaterThan(0);
		assertThat(dnaConsultaDao.countByMutacionIsFalse()).isEqualTo(0);
	}

}
