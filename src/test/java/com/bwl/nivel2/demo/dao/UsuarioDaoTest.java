package com.bwl.nivel2.demo.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.bwl.nivel2.demo.entity.Rol;
import com.bwl.nivel2.demo.entity.Usuario;
@RunWith(SpringRunner.class)
@DataJpaTest
class UsuarioDaoTest {
	@Autowired
    private TestEntityManager entityManager;
	@Autowired
    private UsuarioDao usuarioDao;
	
	
	
	
	@Test
	void test() {
		Usuario usuario = new Usuario();
		usuario.setNombre("Usuario test");
		usuario.setContrasenia("test");
		usuario.setUsuarioId(99);
		usuarioDao.save(usuario);
		Usuario usuarioEncontrado = usuarioDao.findByNombre("Usuario test");
		assertThat(usuario.getUsuarioId()== usuarioEncontrado.getUsuarioId()).isEqualTo(false);
		
	}

}
