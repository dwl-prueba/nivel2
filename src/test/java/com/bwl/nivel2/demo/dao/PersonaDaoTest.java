package com.bwl.nivel2.demo.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.bwl.nivel2.demo.entity.Persona;
@RunWith(SpringRunner.class)
@DataJpaTest
class PersonaDaoTest {
	@Autowired
    private TestEntityManager entityManager;
	@Autowired
    private PersonaDao personaDao;
	
	@Test
	void test() {
		Persona persona = new Persona();
		persona.setAlta(new Date());
		persona.setMutacion(true);
		persona.setNombre("humano 2 test");
		persona.setPersonaId(2);
		personaDao.save(persona);
		assertThat(personaDao.findByMutacionIsTrue().size()).isGreaterThan(0);
		assertThat(personaDao.findByMutacionIsFalse().size()).isEqualTo(0);
		assertThat(personaDao.countByMutacionIsTrue()).isGreaterThan(0);
		assertThat(personaDao.countByMutacionIsFalse()).isEqualTo(0);
	}

}
