package com.bwl.nivel2.demo.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import com.bwl.nivel2.demo.entity.Dna;
import com.bwl.nivel2.demo.entity.Persona;
@RunWith(SpringRunner.class)
@DataJpaTest
class DnaDaoTest {
	@Autowired
    private TestEntityManager entityManager;
	@Autowired
    private DnaDao dnaDao;
	@Autowired
    private PersonaDao personaDao;
	
	
	@Test
	void test() {
		Dna dna = new Dna();
		Persona persona = new Persona();
		persona.setAlta(new Date());
		persona.setPersonaId(1);
		persona.setMutacion(true);
		persona.setNombre("Humano test");
		personaDao.save(persona);
		dna.setDna("ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCACTG");
		dna.setCreacion(new Date());
		dna.setPersonaId(persona);
		entityManager.persist(dna);
		List<Dna> listaDna = dnaDao.findAll();
		assertThat(listaDna.size()).isGreaterThan(0);
	}

}
