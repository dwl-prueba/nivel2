package com.bwl.nivel2.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.bwl.nivel2.demo.dao.PersonaDao;
import com.bwl.nivel2.demo.entity.Persona;

@RunWith(SpringRunner.class)
@SpringBootTest
class IPersonaServiceTest {
	@Autowired
	IPersonaService personaService;
	@MockBean
	PersonaDao personaDao;
	@MockBean
	IGestionDNAService dnaService;
	

	@BeforeEach
	public void runBare() {
		Mockito.when(personaDao.countByMutacionIsTrue()).thenReturn(4);
		Mockito.when(personaDao.countByMutacionIsFalse()).thenReturn(3);
		Persona persona = new Persona();
		persona.setNombre("personaTest");
		List<Persona> personasSinMutar = new ArrayList<>();
		List<Persona> personas = new ArrayList<>();
		personasSinMutar.add(persona);
		personas.add(persona);
		personas.add(persona);
		Mockito.when(personaDao.findAll()).thenReturn(personas);
		Mockito.when(personaDao.findByMutacionIsFalse()).thenReturn(personasSinMutar);
		Mockito.when(personaDao.findByMutacionIsTrue()).thenReturn(personas);
		
	}

	@Test
	void testConteoMutaciones() {
		assertThat(
				personaService.conteoMutaciones().getConMutacion() + personaService.conteoMutaciones().getSinMutacion())
						.isEqualTo(7);
	}
	@Test
	void testFindAllPersonas() {
		assertThat(
				personaService.findAllPersonas().size()).isEqualTo(2);
	}
	@Test
	void testFindPersonasPorMutacionTrue() {
		assertThat(
				personaService.findPersonasPorMutacion(true).size()).isEqualTo(2);
	}
	
	@Test
	void testFindPersonasPorMutacionFalse() {
		assertThat(
				personaService.findPersonasPorMutacion(false).size()).isEqualTo(1);
	}
	
	@Test
	void testNuevaPersona() {
		assertDoesNotThrow(() -> personaService.nuevaPersona("persona test"));
				
	}

}
