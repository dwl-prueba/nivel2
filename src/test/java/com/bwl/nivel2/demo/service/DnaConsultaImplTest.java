package com.bwl.nivel2.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.Before;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.bwl.nivel2.demo.dao.DnaConsultaDao;
import com.bwl.nivel2.demo.dao.UsuarioDao;


@RunWith(SpringRunner.class)
@SpringBootTest
class DnaConsultaImplTest {
	 @Autowired
	    private IDnaConsultaService dnaService;
	 @MockBean
	    private DnaConsultaDao dnaConsultaDao;
	 @MockBean
	 private UsuarioDao usuarioDao;
	 @BeforeEach
	 public void preconfiguradoMockito() {
		 Mockito.when(dnaConsultaDao.countByMutacionIsTrue())
	      .thenReturn(4);
	    Mockito.when(dnaConsultaDao.countByMutacionIsFalse())
	      .thenReturn(6); 
	 }
	 
	 
	@Test
	void test() {
	    assertThat(dnaService.getEstadisticasConsultas().getRatio() == 0.4).isEqualTo(true);
	}

}
