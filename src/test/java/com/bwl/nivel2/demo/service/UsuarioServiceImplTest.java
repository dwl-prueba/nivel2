package com.bwl.nivel2.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.bwl.nivel2.demo.dao.PersonaDao;
import com.bwl.nivel2.demo.dao.UsuarioDao;
import com.bwl.nivel2.demo.entity.Usuario;

@RunWith(SpringRunner.class)
@SpringBootTest
class UsuarioServiceImplTest {
	@Autowired
	IUsuarioService usuarioService;
	@MockBean
	UsuarioDao usuarioDao;
	@BeforeEach
	public void configuracionMock() {
		Usuario usuario = new Usuario();
		usuario.setNombre("Pedro");
		usuario.setUsuarioId(1);
		usuario.setContrasenia("secreto");
		
		Mockito.when(usuarioDao.findByNombre("Pedro"))
	      .thenReturn(usuario);
		Mockito.when(usuarioDao.findByNombreAndContrasenia("Pedro", "secreto"))
	      .thenReturn(usuario);
	}
	
	@Test
	void testGetUsuarioPorNombre() {
		assertThat(usuarioService.getUsuarioPorNombre("Pedro").getUsuarioId()).isEqualTo(1);
	}

	@Test
	void testregistrarUsuario() {
		assertThat(usuarioService.validarUsuario("Pedro", "secreto").getUsuarioId()).isEqualTo(1);
	}

	@Test
	void testvalidarUsuario() {
		Usuario usuario = new Usuario();
		usuario.setNombre("María");
		usuario.setUsuarioId(3);
		usuario.setContrasenia("secreto");
		assertDoesNotThrow(() -> usuarioService.registrarUsuario(usuario));
	}
}
