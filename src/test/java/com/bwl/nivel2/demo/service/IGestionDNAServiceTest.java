package com.bwl.nivel2.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import java.util.ArrayList;
import java.util.List;


import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import com.bwl.nivel2.demo.dao.DnaDao;
import com.bwl.nivel2.demo.dao.PersonaDao;
import com.bwl.nivel2.demo.dao.UsuarioDao;
import com.bwl.nivel2.demo.entity.Persona;
import com.bwl.nivel2.demo.entity.Usuario;
@RunWith(SpringRunner.class)
@SpringBootTest
class IGestionDNAServiceTest {
@Autowired
IGestionDNAService gestionService;

@MockBean
PersonaDao personaDao;
@MockBean
UsuarioDao usuariodao;
@MockBean
DnaDao dnaDao;

@BeforeEach
public void configuracionMock(){
	Persona persona = new Persona();
	persona.setPersonaId(2);
	persona.setNombre("personatest");
	Mockito.when(personaDao.save(persona)).thenReturn(persona);
	Mockito.when(personaDao.countByMutacionIsFalse()).thenReturn(3);
	Usuario usuario = new Usuario();
	Mockito.when(usuariodao.findByNombre("aUserName")).thenReturn(usuario);
}

	@Test
	void testBusquedaMutacionGeneral() {
		List<String> dnas = new ArrayList<String>();
		dnas.add("ATGCGA");
		dnas.add("CAGTGC");
		dnas.add("TTATGT");
		dnas.add("AGAAGG");
		dnas.add("CCCCTA");
		dnas.add("TCACTG");
       assertThat (gestionService.busquedaMutacionGeneral(dnas)).isEqualTo(true);
	}
	@Test
	void testNuevaPersona() {
		assertDoesNotThrow(() -> gestionService.generarDna("ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCACTG"));
	}

}
