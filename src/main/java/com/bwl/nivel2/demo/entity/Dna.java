/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bwl.nivel2.demo.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author yair
 */
@Entity
@Table(name = "dna")
@NoArgsConstructor@Setter@Getter
public class Dna implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dna_id")
    private Integer dnaId;
    @Column(name = "creacion")
    @Temporal(TemporalType.DATE)
    private Date creacion;
    @Size(max = 2147483647)
    @Column(name = "dna")
    private String dna;
@JsonBackReference    
@JoinColumn(name = "persona_id", referencedColumnName = "persona_id")
    @ManyToOne
    private Persona personaId;
    
}
