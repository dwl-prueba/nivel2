package com.bwl.nivel2.demo.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.bwl.nivel2.demo.dao.DnaDao;
import com.bwl.nivel2.demo.dao.PersonaDao;
import com.bwl.nivel2.demo.dao.UsuarioDao;
import com.bwl.nivel2.demo.entity.Dna;
import com.bwl.nivel2.demo.entity.Persona;
import com.bwl.nivel2.demo.entity.Usuario;

import javassist.expr.NewArray;
import net.bytebuddy.dynamic.scaffold.MethodRegistry.Handler.ForImplementation;

@Service
public class GestionDNAServiceImpl implements IGestionDNAService {

    static String A = "A", T = "T", C = "C", G = "G", A4 = A + A + A + A, T4 = T + T + T + T, C4 = C + C + C + C,
            G4 = G + G + G + G;
    static Integer TAMANIO_CADENA_DNA = 6;
    @Autowired
    DnaDao dnaDao;
    @Autowired
    PersonaDao personaDao;
    @Autowired
    UsuarioDao usuariodao;

    @Override
    public Boolean busquedaMutacionGeneral(List<String> dnas) {
        int conteoMutacion = 0;
        String[][] dnasMatriz = new String[dnas.size()][dnas.get(0).length() - 1];
        for (int i = 0; i < dnas.size(); i++) {
            conteoMutacion = conteoMutacion(conteoMutacion, dnas.get(i));
            dnasMatriz[i] = dnas.get(i).split("");
        }
        return busquedaMutacionVerticalYDiagonales(dnasMatriz, 0, conteoMutacion);
    }

    @Override
    public boolean busquedaMutacionVerticalYDiagonales(String[][] dnasMatriz, int numeroBusqueda,
            int conteoMutaciones) {
        String cadennaVertical;
        String cadenaDiagonalDerecha1 = "";
        String cadenaDiagonalDerecha2 = "";
        String cadenaDiagonalIzquierda1 = "";
        String cadenaDiagonalIzquierda2 = "";
        boolean mutacion;
        if (conteoMutaciones > 2) {
            return true;
        } else {
            mutacion = false;
        }
        int idIzquierda = dnasMatriz.length - 1;
        for (int i = 0; i < dnasMatriz.length; i++) {
            try {
                if (numeroBusqueda > 0) {
                    cadenaDiagonalDerecha1 += dnasMatriz[i][i - numeroBusqueda];
                }
                cadenaDiagonalDerecha2 += dnasMatriz[i - numeroBusqueda][i];
                cadenaDiagonalIzquierda1 += dnasMatriz[i][idIzquierda];
                cadenaDiagonalIzquierda2 += dnasMatriz[Math.abs(i - 5)][Math.abs(idIzquierda - 5)];
                --idIzquierda;
            } catch (Exception e) {
            }
            if (numeroBusqueda == 0) {
                cadennaVertical = "";
                for (int j = 0; j < dnasMatriz[i].length; j++) {
                    cadennaVertical += dnasMatriz[j][i];
                }
                conteoMutaciones = conteoMutacion(conteoMutaciones, cadennaVertical);
            }
        }
        conteoMutaciones = conteoMutacion(conteoMutaciones, cadenaDiagonalDerecha1);
        conteoMutaciones = conteoMutacion(conteoMutaciones, cadenaDiagonalDerecha2);
        conteoMutaciones = conteoMutacion(conteoMutaciones, cadenaDiagonalIzquierda1);
        conteoMutaciones = conteoMutacion(conteoMutaciones, cadenaDiagonalIzquierda2);
        numeroBusqueda++;
        if (numeroBusqueda < dnasMatriz.length) {
            return busquedaMutacionVerticalYDiagonales(dnasMatriz, numeroBusqueda, conteoMutaciones);
        } else {
            return mutacion;
        }
    }

    @Override
    public Integer conteoMutacion(Integer conteoMutacion, String dna) {
        if (dna.contains(A4) || dna.contains(T4) || dna.contains(G4) || dna.contains(C4)) {
            conteoMutacion++;
        }
        return conteoMutacion;
    }

    @Override
    public void generarDna(String nombre) {
        List<String> listaDNA = new ArrayList<>();
        Persona persona = new Persona();
        persona.setNombre(nombre);
        String dnaTexto = "";
        String aux;
        Dna dna = new Dna();
        for (int i = 0; i < TAMANIO_CADENA_DNA; i++) {
            aux = generaCadena();
            if (i==0) {
                dnaTexto = aux;
            } else {
                dnaTexto += "," + aux;
            }

            listaDNA.add(aux);
        }
        String nombreUsuario;
        try {
	    nombreUsuario = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        }catch (Exception e) {
        	nombreUsuario ="";
		}
		
			Usuario usuario = usuariodao.findByNombre(nombreUsuario);	
		
		
        
		
        persona.setAlta(new Date());
        persona.setMutacion(busquedaMutacionGeneral(listaDNA));
        persona.setUsuarioIdAlta(usuario);
        dna.setCreacion(new Date());
        dna.setDna(dnaTexto);
        personaDao.save(persona);
        persona =personaDao.save(persona);
        dna.setPersonaId(persona);
        dnaDao.save(dna);
    }

    @Override
    public String generaCadena() {
        int selector;
        String cadena = "";
        while (cadena.length() < TAMANIO_CADENA_DNA) {
            selector = (int) (Math.random() * TAMANIO_CADENA_DNA);
            switch (selector) {
                case 1:
                    cadena += A;
                    break;
                case 2:
                    cadena += T;
                    break;
                case 3:
                    cadena += C;
                    break;
                case 4:
                    cadena += G;
                    break;
            }
        }
        return cadena;
    }

}
