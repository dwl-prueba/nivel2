package com.bwl.nivel2.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bwl.nivel2.demo.dao.UsuarioDao;
import com.bwl.nivel2.demo.entity.Usuario;
@Service
public class UsuarioServiceImpl implements IUsuarioService {
	@Autowired
	UsuarioDao usuarioDao;

	@Override
	public void registrarUsuario(Usuario usuario) {
		usuarioDao.save(usuario);
	}

	@Override
	public Usuario validarUsuario(String nombre, String contrasenia) {
		return usuarioDao.findByNombreAndContrasenia(nombre, contrasenia);
	}

	@Override
	public Usuario getUsuarioPorNombre(String nombre) {
		return usuarioDao.findByNombre(nombre);
	}

}
