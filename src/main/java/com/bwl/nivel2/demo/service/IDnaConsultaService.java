package com.bwl.nivel2.demo.service;

import java.util.List;

import com.bwl.nivel2.demo.dto.ConteoEstadisticaConsultasDna;

public interface IDnaConsultaService {

	public abstract ConteoEstadisticaConsultasDna getEstadisticasConsultas();
	public abstract void saveConsultaDna(List<String> dna, Boolean mutacion);
	
}
