package com.bwl.nivel2.demo.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.bwl.nivel2.demo.dao.DnaConsultaDao;
import com.bwl.nivel2.demo.dao.UsuarioDao;
import com.bwl.nivel2.demo.dto.ConteoEstadisticaConsultasDna;
import com.bwl.nivel2.demo.entity.DnaConsulta;
import com.bwl.nivel2.demo.entity.Usuario;
@Service
public class DnaConsultaImpl implements IDnaConsultaService {

	@Autowired
	DnaConsultaDao dnaConsultaDao;
	@Autowired
	UsuarioDao usuarioDao;
	
	@Override
	public ConteoEstadisticaConsultasDna getEstadisticasConsultas() {
		
		ConteoEstadisticaConsultasDna conteo = new ConteoEstadisticaConsultasDna();
		conteo.setCount_mutations(dnaConsultaDao.countByMutacionIsTrue());
		conteo.setCount_no_mutations(dnaConsultaDao.countByMutacionIsFalse());
		double total =conteo.getCount_mutations()+conteo.getCount_no_mutations();
		conteo.setRatio(conteo.getCount_mutations()/total);
		return conteo;
	}

	@Override
	public void saveConsultaDna(List<String> dna, Boolean mutacion) {
		DnaConsulta dnaConsulta = new DnaConsulta();
		String cadenaDna ="";
		
		for (String elementodna : dna) {
			if(cadenaDna.equals("")) {
				cadenaDna=elementodna;
			}else {
				cadenaDna+=","+elementodna;
			}	
		}
		String nombreUsuario = SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
		Usuario usuario = usuarioDao.findByNombre(nombreUsuario);
		dnaConsulta.setDna(cadenaDna);
		dnaConsulta.setFechaAlta(new Date());
		dnaConsulta.setMutacion(mutacion);
		dnaConsulta.setUsuarioAlta(usuario);
		dnaConsultaDao.save(dnaConsulta);
	}

}
