package com.bwl.nivel2.demo.service;

import java.util.HashMap;
import java.util.List;

import com.bwl.nivel2.demo.dto.ConteoMutaciones;
import com.bwl.nivel2.demo.entity.Persona;

public interface IPersonaService {
public abstract List<Persona> findAllPersonas();
public abstract List<Persona> findPersonasPorMutacion(Boolean mutacion);
public abstract void nuevaPersona(String nombre);
public abstract ConteoMutaciones conteoMutaciones();
}
