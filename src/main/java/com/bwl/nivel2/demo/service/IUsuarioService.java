package com.bwl.nivel2.demo.service;

import com.bwl.nivel2.demo.entity.Usuario;

public interface IUsuarioService {
public  abstract void registrarUsuario(Usuario usuario);
public  abstract Usuario validarUsuario(String nombre, String contrasenia);
public  abstract Usuario getUsuarioPorNombre(String nombre);
	
}
