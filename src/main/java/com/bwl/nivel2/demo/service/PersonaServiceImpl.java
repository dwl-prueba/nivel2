package com.bwl.nivel2.demo.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bwl.nivel2.demo.dao.PersonaDao;
import com.bwl.nivel2.demo.dto.ConteoMutaciones;
import com.bwl.nivel2.demo.entity.Persona;

@Service
public class PersonaServiceImpl implements IPersonaService {

	@Autowired
	PersonaDao personaDao;
	@Autowired
	IGestionDNAService dnaService;

	@Override
	public List<Persona> findAllPersonas() {
		return personaDao.findAll();
	}

	@Override
	public List<Persona> findPersonasPorMutacion(Boolean mutacion) {
		if (mutacion) {
			return personaDao.findByMutacionIsTrue();
		} else {
			return personaDao.findByMutacionIsFalse();
		}
	}

	@Override
	public void nuevaPersona(String nombre) {
		dnaService.generarDna(nombre);
	}

	@Override
	public ConteoMutaciones conteoMutaciones() {
		ConteoMutaciones conteoMutacion = new ConteoMutaciones();
		conteoMutacion.setConMutacion(personaDao.countByMutacionIsTrue());
		conteoMutacion.setSinMutacion(personaDao.countByMutacionIsFalse());
		return conteoMutacion;
	}
}
