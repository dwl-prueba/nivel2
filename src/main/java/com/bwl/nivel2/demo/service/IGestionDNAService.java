package com.bwl.nivel2.demo.service;

import java.util.HashMap;
import java.util.List;

import com.bwl.nivel2.demo.entity.Persona;
//codigo fuente para nivel 2
public interface IGestionDNAService {
public abstract Boolean busquedaMutacionGeneral(List<String> dnas);
public abstract boolean busquedaMutacionVerticalYDiagonales(String[][] dnasMatriz, int numeroBusqueda, int conteoMutaciones);
public abstract Integer conteoMutacion(Integer conteoMutacion, String dna);
public abstract void generarDna(String nombre);
public abstract String generaCadena();
}
