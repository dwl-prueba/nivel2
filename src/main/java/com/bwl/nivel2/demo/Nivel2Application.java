package com.bwl.nivel2.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;



@SpringBootApplication
@EnableAutoConfiguration
@EnableTransactionManagement
public class Nivel2Application {

	public static void main(String[] args) {
		SpringApplication.run(Nivel2Application.class, args);
	}

}
