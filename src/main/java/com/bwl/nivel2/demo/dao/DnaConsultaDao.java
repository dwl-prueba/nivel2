/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bwl.nivel2.demo.dao;

import com.bwl.nivel2.demo.entity.DnaConsulta;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author yair
 */
public interface DnaConsultaDao extends JpaRepository<DnaConsulta, Integer>{
 public abstract Integer countByMutacionIsTrue();
 public abstract Integer countByMutacionIsFalse();
 
 
}
