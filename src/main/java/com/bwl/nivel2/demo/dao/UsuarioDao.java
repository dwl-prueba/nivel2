package com.bwl.nivel2.demo.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bwl.nivel2.demo.entity.Usuario;

public interface UsuarioDao extends JpaRepository<Usuario, Integer> {
Usuario findByNombreAndContrasenia(String nombre, String contrasenia);
Usuario findByNombre(String nombre);

}
