package com.bwl.nivel2.demo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor@Setter@Getter
public class ConteoEstadisticaConsultasDna {
Integer count_mutations,count_no_mutations;
Double ratio;
}
