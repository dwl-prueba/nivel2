package com.bwl.nivel2.demo.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor@Setter@Getter
public class ConteoMutaciones {
	Integer conMutacion, sinMutacion;
}
