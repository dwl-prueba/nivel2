package com.bwl.nivel2.demo.controller;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bwl.nivel2.demo.dto.ConteoMutaciones;
import com.bwl.nivel2.demo.entity.Persona;
import com.bwl.nivel2.demo.service.IPersonaService;

@RestController
@RequestMapping("/persona")
// servicios rest para el nivel 4
public class PersonaController {
	@Autowired
	IPersonaService personaService;

	@GetMapping("/todas")
	public List<Persona> getAllPersona() {
		return personaService.findAllPersonas();
	}
	@GetMapping("/mutacion/{mutacion}")
	public List<Persona> getAllPersona(@PathVariable Boolean mutacion) {
		return personaService.findPersonasPorMutacion(mutacion);
	}
	@PostMapping("")
	public void guardaPersona(@RequestBody HashMap<String, String> nombreHumano) {
			personaService.nuevaPersona(nombreHumano.get("nombre"));
	}
	@GetMapping("/mutacion/conteo")
	public ConteoMutaciones getConteo() {
		return personaService.conteoMutaciones();
	}
}
