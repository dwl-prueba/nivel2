package com.bwl.nivel2.demo.controller;

import java.util.ArrayList;
import java.util.HashMap;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bwl.nivel2.demo.dto.ConteoEstadisticaConsultasDna;
import com.bwl.nivel2.demo.service.IDnaConsultaService;
import com.bwl.nivel2.demo.service.IGestionDNAService;

@RestController
// Servicios rest para el nivel 2 y 3
public class MutacionController {
	@Autowired
	IGestionDNAService servicioDNA;
	@Autowired
	IDnaConsultaService dnaService;

	
	@PostMapping("/mutation")
	public ResponseEntity<?> VerificarMutacion(@RequestBody HashMap<String, ArrayList<String>> dna) {
		if (servicioDNA.busquedaMutacionGeneral(dna.get("dna"))) {
			dnaService.saveConsultaDna(dna.get("dna"), true);
			return ResponseEntity.ok(null);
		} else {
			dnaService.saveConsultaDna(dna.get("dna"), false);
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body(null);
		}
	}
	@GetMapping("/stats")
	public ConteoEstadisticaConsultasDna getEstadisticasCOnsultas() {
		return dnaService.getEstadisticasConsultas();
	}
	
}
