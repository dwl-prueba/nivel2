package com.bwl.nivel2.demo.config;

import com.bwl.nivel2.demo.entity.Usuario;
import com.bwl.nivel2.demo.service.IUsuarioService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.io.IOException;
import java.security.Key;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.util.Base64Utils;

@Component
public class JWTServiceImpl implements JWTService {

	//public static final String SECRETO = Base64Utils.encodeToString(
		//	"Alguna.Clave.Secreta.123456.Alguna.Clave.Secreta.123456.Alguna.Clave.Secreta.123456".getBytes());
	public static final long TIEMPO_EXPIRACION = 14000000L;
	public static final String PREFIJO_TOKEN = "Bearer ";
	public static final String HEADER_STRING = "Authorization";
	public static final Key SECRET_KEY = Keys.secretKeyFor(SignatureAlgorithm.HS512);
	@Autowired
	IUsuarioService usuarioService;
	
	
	@Override
	public String crear(Authentication auth) throws JsonProcessingException {
		String username = ((User) auth.getPrincipal()).getUsername();

		Collection<? extends GrantedAuthority> roles = auth.getAuthorities();
		Claims claims = Jwts.claims();
		claims.put("authorities", new ObjectMapper().writeValueAsString(roles));
        String token = Jwts.builder()
                .setClaims(claims)
                .setSubject(username)
                .signWith(SECRET_KEY)
				.setExpiration(new Date(System.currentTimeMillis() + TIEMPO_EXPIRACION)).compact();
		return token;
	}

	@Override
	public boolean validar(String token) {
		try {
			getClaims(token);
			return true;
		} catch (JwtException | IllegalArgumentException e) {
			return false;
		}
	}

	@Override
	public Claims getClaims(String token) {
        Claims claims = Jwts.parser()
       .setSigningKey(SECRET_KEY)
       .parseClaimsJws(resolverToken(token)).getBody();
		
		return claims;
	}

	@Override
	public String getUsuario(String token) {
		return getClaims(token).getSubject();
	}

	@Override
	public Collection<? extends GrantedAuthority> getRoles(String token) throws IOException {
		Object roles = getClaims(token).get("authorities");
		Collection<? extends GrantedAuthority> authorities = Arrays
				.asList(new ObjectMapper().addMixIn(SimpleGrantedAuthority.class, SimpleGrantedAuthorityMixin.class)
						.readValue(roles.toString().getBytes(), SimpleGrantedAuthority[].class));
		return authorities;
	}

	@Override
	public String resolverToken(String token) {
		if (token != null && token.startsWith(PREFIJO_TOKEN)) {
			return token.replace(PREFIJO_TOKEN, "");
		}
		return null;
	}

	@Override
	public int getIdUsuario(String token) {
		return (int) getClaims(token).get("idUsuario");
	}

	@Override
	public Usuario getNombreCompletoUsuario(Authentication auth){
		String username = ((User) auth.getPrincipal()).getUsername();
		Usuario usuario = usuarioService.getUsuarioPorNombre(username);
		usuario.setContrasenia(null);
		return usuario;
	}

	

}