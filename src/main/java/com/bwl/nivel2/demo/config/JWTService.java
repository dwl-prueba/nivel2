package com.bwl.nivel2.demo.config;
import com.bwl.nivel2.demo.entity.Usuario;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.jsonwebtoken.Claims;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

public interface JWTService {
    public String crear(Authentication auth) throws JsonProcessingException;
    public boolean validar(String token);
    public Claims getClaims(String token);
    public String getUsuario(String token);
    public int getIdUsuario(String token);
    public Collection<? extends GrantedAuthority> getRoles(String token) throws IOException;
    public String resolverToken(String token);
    public Usuario getNombreCompletoUsuario(Authentication auth);
 
}
