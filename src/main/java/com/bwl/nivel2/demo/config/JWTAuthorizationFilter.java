package com.bwl.nivel2.demo.config;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;


public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

    private JWTService jwtService;

    public JWTAuthorizationFilter(AuthenticationManager authenticationManager, JWTService jwtService) {
        super(authenticationManager);
        this.jwtService = jwtService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        String header = request.getHeader(JWTServiceImpl.HEADER_STRING);

        if (!requieresAuthorization(header)) {
            chain.doFilter(request, response);
            return;
        }
        
        
        UsernamePasswordAuthenticationToken autenticacion = null;
        if (jwtService.validar(header)) {
            autenticacion = new UsernamePasswordAuthenticationToken(jwtService.getUsuario(header), null, jwtService.getRoles(header));
        }

        SecurityContextHolder.getContext().setAuthentication(autenticacion);
        chain.doFilter(request, response);
    }

    protected boolean requieresAuthorization(String header) {
        if (header == null || !header.startsWith(JWTServiceImpl.PREFIJO_TOKEN)) {
            return false;
        }
        return true;
    }

}
