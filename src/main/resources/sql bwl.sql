-- public.rol definition

-- Drop table

-- DROP TABLE public.rol;

CREATE TABLE public.rol (
	rol_id serial NOT NULL,
	nombre varchar NULL,
	CONSTRAINT rol_pk PRIMARY KEY (rol_id)
);


-- public.usuario definition

-- Drop table

-- DROP TABLE public.usuario;

CREATE TABLE public.usuario (
	usuario_id serial NOT NULL,
	nombre varchar(10) NULL,
	contrasenia varchar(100) NULL,
	rol_id int4 NULL,
	CONSTRAINT usuario_pk PRIMARY KEY (usuario_id),
	CONSTRAINT usuario_fk FOREIGN KEY (rol_id) REFERENCES rol(rol_id)
);


-- public.dna_consulta definition

-- Drop table

-- DROP TABLE public.dna_consulta;

CREATE TABLE public.dna_consulta (
	dna_consulta_id serial NOT NULL,
	dna text NULL,
	mutacion bool NULL,
	usuario_alta int4 NULL,
	fecha_alta timestamp NULL,
	CONSTRAINT dna_consulta_pk PRIMARY KEY (dna_consulta_id),
	CONSTRAINT dna_consulta_fk FOREIGN KEY (usuario_alta) REFERENCES usuario(usuario_id)
);


-- public.persona definition

-- Drop table

-- DROP TABLE public.persona;

CREATE TABLE public.persona (
	nombre text NULL,
	alta date NULL,
	persona_id serial NOT NULL,
	mutacion bool NULL,
	usuario_id_alta int4 NULL,
	CONSTRAINT persona_pk PRIMARY KEY (persona_id),
	CONSTRAINT persona_fk FOREIGN KEY (usuario_id_alta) REFERENCES usuario(usuario_id)
);


-- public.dna definition

-- Drop table

-- DROP TABLE public.dna;

CREATE TABLE public.dna (
	dna_id serial NOT NULL,
	creacion date NULL,
	persona_id int4 NULL,
	dna text NULL,
	CONSTRAINT dna_pk PRIMARY KEY (dna_id),
	CONSTRAINT dna_fk FOREIGN KEY (persona_id) REFERENCES persona(persona_id)
);

INSERT INTO public.rol
(rol_id, nombre)
VALUES(1, 'ROLE_USER');


INSERT INTO public.usuario
(usuario_id, nombre, contrasenia, rol_id)
VALUES(1, 'admin', '$2a$10$OshyiGJruRabGUBiLwT7Hub4DWXWbEcYG.DP9Kd4rE/fzNsp8ObeW', 1);

INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Manuel', '2020-10-22', 12, true, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Carlos', '2020-10-22', 13, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Luis', '2020-10-21', 7, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Eduardo', '2020-10-21', 8, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Ana', '2020-10-21', 9, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Pedro', '2020-10-21', 6, true, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Martha', '2020-10-21', 11, true, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('María', '2020-10-21', 10, true, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Fabian', '2020-10-22', 23, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Juan', '2020-10-22', 14, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Yael', '2020-10-22', 15, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Elvis', '2020-10-22', 16, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Yadira', '2020-10-22', 17, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Karina', '2020-10-22', 18, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Elmer', '2020-10-22', 19, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Daniel', '2020-10-22', 20, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Cesar', '2020-10-22', 21, false, 1);
INSERT INTO public.persona
(nombre, alta, persona_id, mutacion, usuario_id_alta)
VALUES('Arturo', '2020-10-22', 22, false, 1);

INSERT INTO public.dna_consulta
(dna_consulta_id, dna, mutacion, usuario_alta, fecha_alta)
VALUES(1, 'ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCACTG', true, NULL, '2020-10-22 03:28:02.666');
INSERT INTO public.dna_consulta
(dna_consulta_id, dna, mutacion, usuario_alta, fecha_alta)
VALUES(2, 'ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCACTG', true, NULL, '2020-10-22 03:28:25.137');

INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(7, '2020-10-21', 9, 'CCGTGC,CTATAC,AATCGT,CTGGCC,CATACC,CTACCT');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(8, '2020-10-21', 10, 'CAATGG,TTACAG,TTAGTT,GCCAAA,GACCCT,CACCAT');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(9, '2020-10-21', 11, 'ATGTCT,ACTCTT,GTGAGA,GCTCAC,GGGGAC,AATGGA');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(10, '2020-10-22', 12, 'GCAATT,TAGCGC,ACTTTT,TGGGTC,GATCGG,AAGTCG');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(11, '2020-10-22', 13, 'TTGTGG,ATGCTT,GCCTCC,CGATGA,CAGGAA,CGATCA');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(4, '2020-10-21', 6, 'CGGGAT,AAAATGA,CCCCTC,GACCCG,TCCAGC,GATAGT');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(5, '2020-10-21', 7, 'CGGGAT,AAAATGA,CCCCTC,GACCCG,TCCAGC,GATAGT');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(6, '2020-10-21', 8, 'CGGGAT,AAAATGA,CCCCTC,GACCCG,TCCAGC,GATAGT');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(12, '2020-10-22', 14, 'GTGTTG,ACACAG,GCGATC,TAAATC,TCGAAT,AAGACA');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(13, '2020-10-22', 15, 'CAACTT,AGTATC,CCGTAA,CCCAGA,ATCTGG,GGCTCC');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(14, '2020-10-22', 16, 'TTACGT,ACAGGT,CAACCG,GTGTTA,TAACCC,TGAAAA');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(15, '2020-10-22', 17, 'TGTACT,GTTCAA,TGTAGA,TCAAAG,TGTACG,TAGTCT');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(16, '2020-10-22', 18, 'CATAGT,TGACCA,ATATGA,AACATC,GCGAAT,CTCGAC');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(17, '2020-10-22', 19, 'CGCGTG,ATGCAA,CTTTCT,TCATAA,AGTTCA,CGGGCT');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(18, '2020-10-22', 20, 'TCAGCG,GGTCCG,GTACCT,GAGGTC,CGTTCG,TTTATA');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(19, '2020-10-22', 21, 'TCACTT,CGTTGG,AAAGCC,TTGGCT,CCACGT,TAGGTG');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(20, '2020-10-22', 22, 'AGGTTG,GAGTAC,TTGGCA,CATCCG,TGGCAC,TTCTTA');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(21, '2020-10-22', 23, 'GATTGT,TAATGC,TCAAAT,CATTCG,TTAGCG,ATCTTG');
INSERT INTO public.dna
(dna_id, creacion, persona_id, dna)
VALUES(22, '2020-10-22', NULL, 'TGAACG,CCATAA,ATAGTT,GTTACG,CAAATC,CTAGAG');


